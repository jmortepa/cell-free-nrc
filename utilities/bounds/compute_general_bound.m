function [r_d, r_d_perfect] = compute_general_bound(psim)
%COMPUTE_NEW_BOUND Summary of this function goes here
%   Detailed explanation goes here
td = (psim.coherence_interval - psim.tu);

r_d = zeros(psim.K, psim.realizations);
r_d_perfect = zeros(psim.K, psim.realizations);

for realization=1:psim.realizations
    disp(['Realization: ' num2str(realization)]);
    alpha = sqrt(psim.rho_d * psim.etas(:, :, realization));
    first_term = zeros(psim.channel_realizations, psim.K);

    a_j_g_k = zeros(psim.channel_realizations, psim.K, psim.K);
    for channel_realization=1:psim.channel_realizations
        psim = gen_channel_realization(psim, realization);
        precoder = alpha .* conj(psim.G_dl_est);
        for k=1:psim.K
            a_k = precoder(k, :).';
            g_k = psim.G_dl(k, :).';

            a_k_j = precoder;
            a_k_j(k, :) = [];

            a_j_g_k(channel_realization, :, k) = precoder * g_k;

            numerator = abs(a_k.' * g_k) .^ 2;
            denominator = sum(abs(a_k_j * g_k) .^ 2) + 1;

            first_term(channel_realization, k) = log2(1 + ...
                numerator / denominator);
        end
    end
    for k=1:psim.K
        var_a_j_g_k = var(a_j_g_k(:, :, k));
        second_term_est = sum(log2(1 + td * var_a_j_g_k)) / td;
% 
%         self_interference = ( (1 + psim.var_alpha_trace) * ...
%             sum(psim.etas(k, :, realization) .* ...
%             psim.betas(k, :, realization) .* ...
%             psim.gammas(k, :, realization)) + ...
%             (1 + psim.var_a_prime) * psim.sum_r_cm * ...
%             sum(psim.etas(k, :, realization) .* ...
%             psim.gammas(k, :, realization) .^ 2) + ...
%             psim.var_a_prime * ...
%             sum(sqrt(psim.etas(k, :, realization)) .* ...
%             psim.gammas(k, :, realization)) .^ 2 ) * psim.rho_d;
% 
%         etas_nk = psim.etas(:, :, realization);
%         etas_nk(k, :) = [];
%         gammas_nk = psim.etas(:, :, realization);
%         gammas_nk(k, :) = [];
% 
%         inter_interference = ( (1 + psim.var_alpha_trace) * ...
%             (etas_nk .* gammas_nk) * psim.betas(k, :, realization).' ) * ...
%             psim.rho_d;
% 
%         w = psim.etas(:, :, realization) .* ...
%             psim.gammas(:, :, realization);
% 
%         var_e = psim.rho_d * w * psim.betas(k, :, realization).';
% 
%         second_term_eq_new = sum(log2(1 + td * var_e)) / td;
% 
%         second_term = (log2(1 + td * self_interference) + ...
%             sum(log2(1 + td * inter_interference))) / td;
%         disp(second_term)
%         disp(second_term_est)
%         disp(second_term_eq_new)

        r_d(k, realization) = mean(first_term(:, k)) - second_term_est;
        r_d_perfect(k, realization) = mean(first_term(:, k));
    end
end

r_d = r_d(:);
r_d_perfect = r_d_perfect(:);

end

