function [psim] = gen_distances(psim)
%GEN_DISTANCES Summary of this function goes here
%   Detailed explanation goes here

    x_ues = psim.max_distance * rand(psim.K, psim.realizations);
    y_ues = psim.max_distance * rand(psim.K, psim.realizations);
           
    if psim.N == 1
        x_aps = (psim.max_distance / 2) * ones(psim.N, psim.realizations);
        y_aps = (psim.max_distance / 2) * ones(psim.N, psim.realizations);
    else
        x_aps = psim.max_distance * rand(psim.N, psim.realizations);
        y_aps = psim.max_distance * rand(psim.N, psim.realizations);
    end
    
    % Duplicate distances for the antennas in the same AP
    x_aps = kron(x_aps, ones(psim.M / psim.N, 1));
    y_aps = kron(y_aps, ones(psim.M / psim.N, 1));
    
    distances_ue_ap = zeros(psim.K, psim.M, psim.realizations);
    distances_ap_ap = zeros(psim.M, psim.M, psim.realizations);
    distances_ue_ue = zeros(psim.K, psim.K, psim.realizations);
    
    for i=1:psim.realizations
        p_aps = [x_aps(:, i) y_aps(:, i)];
        p_ues = [x_ues(:, i) y_ues(:, i)];
        
        distances_ue_ap(:, :, i) = pdist2(p_ues, p_aps);
        distances_ap_ap(:, :, i) = pdist2(p_aps, p_aps);
        distances_ue_ue(:, :, i) = pdist2(p_ues, p_ues);  
    end
    
    psim.distances_ue_ap = distances_ue_ap;
    psim.distances_ap_ap = distances_ap_ap;
    psim.distances_ue_ue = distances_ue_ue;
end

