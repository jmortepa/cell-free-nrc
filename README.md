# Channel Non-Reciprocity in Cell-Free

## Project organization

The project is organized in the following directories

* /docs
* /simulations
* /utilities
	* /setters
	* /generators

The folder `simulations` includes the scripts used to obtain all the results, both numeric and graphic.
The folder `utilities` includes some useful functions used in several scenarios, there are two sub-folders,
`setters` and `generators`, which include functions to change easily some parameters and generate realizations
based of certain parameters, respectively. Finally, the folder `docs` includes documentation about the problem
formulation and derivations, as well as some conclusions about the results obtained in the simulations