function [psim] = gen_channel_parameters(psim)
%GEN_CHANNEL_PARAMETERS Summary of this function goes here
%   Detailed explanation goes here
    if strcmp(psim.channel_model, 'simple')
        psim.distances = psim.max_distance * ...
            ones(psim.K, psim.M, psim.realizations);
        psim.betas = 1 ./ (psim.distances .^ 3.8);
    elseif strcmp(psim.channel_model, 'hata-cost231')
        % Path loss calculation: Three ranges
        range_one = psim.distances_ue_ap > psim.d1;
        range_two = psim.d0 < psim.distances_ue_ap <= psim.d1;
        range_three = psim.distances_ue_ap <= psim.d0;
        
        L = 46.3 + 33.9 * log10(psim.frequency) - ... 
            13.82 * log10(psim.h_ap) - ...
            (1.1 * log10(psim.frequency) - 0.7) * psim.h_ue + ...
            (1.56 * log10(psim.frequency) - 0.8);
        
        path_losses = zeros(psim.K, psim.M, psim.realizations);
        
        path_losses(range_one) = -L - ...
            35 * log10(psim.distances_ue_ap(range_one) / 1000);
        path_losses(range_two) = -L - ...
            15 * log10(psim.d1 / 1000) - ...
            20 * log10(psim.distances_ue_ap(range_two) / 1000);
        path_losses(range_three) = -L - ...
            15 * log10(psim.d1 / 1000) - 20 * log10(psim.d0 / 1000);
        
        path_losses = 10 .^ (path_losses / 10);
        
        % Shadowing calculation      
        if strcmp(psim.shadowing_model, 'correlated')
            % Covariance shadowing matrices
            cov_a = 2 .^ (-psim.distances_ap_ap / psim.d_decorr);
            cov_b = 2 .^ (-psim.distances_ue_ue / psim.d_decorr);
            
            z = zeros(psim.K, psim.M, psim.realizations);
            for i=1:psim.realizations
                a = mvnrnd(zeros(1, psim.M), cov_a(:, :, i));
                b = mvnrnd(zeros(1, psim.K), cov_b(:, :, i));
                
                a_prime = repmat(a, [psim.K 1]);
                b_prime = repmat(b.', [1 psim.M]);

                z(:, :, i) = sqrt(psim.delta_shadow) * a_prime + ...
                    sqrt(1 - psim.delta_shadow) * b_prime;
            end
        elseif strcmp(psim.shadowing_model, 'uncorrelated')
            z = randn(psim.K, psim.M, psim.realizations);
        else
            error('Shadowing model not contemplated');
        end
        
        % Note that when dmk <= d1, there is no shadowing
        z(psim.distances_ue_ap <= psim.d1) = 0;
        
        % Betas calculation
        psim.betas = path_losses .* 10 .^ (psim.sigma_shadow .* z ./ 10);
    else
        error('Channel model unknown');
    end
    
    % MMSE estimate variance
    psim.gammas = ( psim.tu * psim.rho_u * (psim.betas .^ 2) ) ./ ...
        (psim.tu * psim.rho_u * psim.betas + 1);
%     psim.etas = zeros(psim.K, psim.M, psim.realizations);
%     for i=1:psim.realizations
%         gammas = squeeze(psim.gammas(:, :, i));
%         betas = squeeze(psim.betas(:, :, i));
%         
%         t = optimvar('t', 'LowerBound', 0);
%         et = optimvar('eta', psim.K, psim.M, 'LowerBound', 0);
%         prob = optimproblem();
%         
%         prob.Objective = -t;
%         
%         denominator = (sum(betas, 2) + 1 / psim.rho_d);
%         
%         sum2one = sum(gammas .* et, 1) == 1;
%         ineq = t - sum(gammas .* sqrt(et), 2) .^ 2 ./ denominator <= 0;
%         
%         prob.Constrains.sum2one = sum2one;
%         prob.Constrains.ineq = ineq;
%         
%         showproblem(prob);
%     end
    
%     for i=1:psim.realizations
%         gammas = squeeze(psim.gammas(:, :, i));
%         betas = squeeze(psim.betas(:, :, i));
%         Aeq = [];
%         for m=1:psim.M
%             Aeq = blkdiag(Aeq, gammas(:, m).');
%         end
%         
%         Aeq = [zeros(psim.M, 1) Aeq];
%         Aeq = [zeros(1, psim.M * psim.K + 1); Aeq];
%         beq = [0; ones(psim.M, 1)];
%         
%         fun = @(x)(-x(1));
% 
%         nonlcon = gen_nlcf(gammas, betas, psim.rho_d, 1, psim.K, psim.M);
%         options = optimoptions('fmincon',...
%             'UseParallel', true, 'MaxFunctionEvaluations', 10000, ...
%             'Algorithm', 'active-set', 'Display', 'notify');
%         
%         est_gammas = repmat(1 ./ sum(gammas, 1), [psim.K 1]);
%         x0 = [3; est_gammas(:)];
%         
%         x = fmincon(fun, x0, [], [], Aeq, beq, [], [], nonlcon, options);
%         psim.etas(:, :, i) = reshape(x(2:end), [psim.K psim.M]);
%     end
%     psim.etas = zeros(psim.K, psim.M, psim.realizations);
%     for i=1:psim.realizations
%         gammas = squeeze(psim.gammas(:, :, i));
%         betas = squeeze(psim.betas(:, :, i));
%         
%         sinr_f = gen_fun_sinr(gammas, betas, psim.rho_d, 1, psim.K, psim.M);
%         Aeq = [];
%         for m=1:psim.M
%             Aeq = blkdiag(Aeq, gammas(:, m).');
%         end
%         beq = ones(psim.M, 1);
%         est_gammas = - repmat(1 ./ sum(gammas, 1), [psim.K 1]);
%         x0 = est_gammas(:);
% %         nl = gen_ceq(gammas, psim.K, psim.M);
%         options = optimoptions('fminimax', 'Display', 'notify',...
%             'UseParallel', true);
%         x = fminimax(sinr_f, x0, [], [], Aeq, beq,...
%             [], zeros(psim.K * psim.M, 1), [], options);
%         psim.etas(:, :, i) = reshape(x, [psim.K psim.M]);
% %         psim.etas(:, :, i) = -psim.etas(:, :, i);
%     end
    
    % No power control. Full power transmission
    if strcmp(psim.power_control, 'per-user')
        psim.etas = repmat(1 ./ sum(psim.gammas, 2), [1 psim.M 1]);
    elseif strcmp(psim.power_control, 'per-ap')
        psim.etas = repmat(1 ./ sum(psim.gammas, 1), [psim.K 1 1]);
     else
        error('Power control unknown');
    end
end

