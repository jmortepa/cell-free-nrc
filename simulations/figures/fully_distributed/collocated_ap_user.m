clear all
clc

% Parameters for the simulation
psim = struct;

% Simulation parameters
psim.realizations = 1;
psim.channel_realizations = 1000;
psim.time_samples = 300;
psim.coherence_interval = 196; % [Samples]
psim.progress_bar = 0;

% MIMO parameters
psim.K = 20; % Number of UEs
psim.M = 100; % Number of total antennas in APs
psim.N = 1; % Number of APs

psim.power_control = 'per-ap';

% Uplink & Downlink parameters
psim.tu = psim.K; % Pilot time [Samples]

% Channel parameters
psim.channel_model = 'simple';
psim.max_distance = 1;

% NRC parameters sweep
var_a_diag_db = [-Inf -Inf -15];
var_c_diag_db = [-Inf -15 -Inf];
var_c_off_db =  [-Inf -Inf -Inf];
cov_c_diag_db = [-Inf -Inf -Inf];

% SNR sweep
rho_d_db = linspace(-20, 30, 10);

% Normalized transmit SNRs
psim = set_rhos_db(psim, 0, 0);

% Initialization
r_d_vec = zeros(length(rho_d_db), length(var_a_diag_db));

% Generate Betas and Gammas
psim = gen_channel_parameters(psim);

for i=1:length(rho_d_db)
    for j=1:length(var_a_diag_db)
        % Normalized transmit SNRs
        psim = set_rhos_db(psim, rho_d_db(i), 0);
        
        % NRC parameters
        % var_a_diag_db, var_c_diag_db, var_c_off_db, cov_c_diag_db
        psim = set_var_nrc(psim, var_a_diag_db(j), var_c_diag_db(j),...
            var_c_off_db(j), cov_c_diag_db(j));
        
        % Compute analytic SINR
        [~, r_d] = compute_analytic_uatf(psim);

        r_d_vec(i, j) = mean(r_d);
   end
end

line_styles = {'k-', 'k--', 'k:', 'k-.', 'kx'};

legends = {};
for i=2:length(var_c_diag_db)
    legends{end + 1} = ['\sigma_{c}^2 = ' ...
        num2str(var_c_diag_db(i)) ' dB, \sigma_{a}^2 = ' ...
        num2str(var_a_diag_db(i)) ];
end

figure
hold on
grid on
for n=2:length(var_c_diag_db)
    plot(rho_d_db, 100 * (r_d_vec(:, 1) - ...
        r_d_vec(:, n)) ./ r_d_vec(:, 1), line_styles{n});
end
% ylim([0 40])
xlabel('SNR (dB)')
ylabel('Spectral Efficiency Degradation (%)')
legend(legends, 'Location', 'northwest')
saveas(gcf, './figures/fully_distributed/collocated_ap_user/collocated_ap_user.eps', ...
    'epsc')
