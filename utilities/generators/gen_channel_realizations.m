function [psim] = gen_channel_realizations(psim)
%GEN_CHANNEL_REALIZATIONS Summary of this function goes here
%   Detailed explanation goes here
    psim.G_ul_error = sqrt((permute(psim.betas - psim.gammas, [2 1 3])) / 2).* ...
        (randn(psim.M, psim.K, psim.realizations) + ...
        1i*randn(psim.M, psim.K, psim.realizations));
    
    psim.G_ul_est = sqrt((permute(psim.gammas, [2 1 3])) / 2) .* ...
        (randn(psim.M, psim.K, psim.realizations) + ...
        1i*randn(psim.M, psim.K, psim.realizations));
    
    psim.G_ul = psim.G_ul_est + psim.G_ul_error;
    
    psim.G_dl_est = permute(psim.G_ul_est, [2 1 3]);
    
    psim.G_dl = zeros(psim.K, psim.M, psim.realizations);
    
    for i=1:psim.realizations
        A_prime = diag(wgn(psim.K, 1, psim.var_a_prime_db, 'complex'));
        A = eye(psim.K) + A_prime;
        
        C_prime = [];
        cov_C_prime_diag = (ones(psim.M/psim.N) - eye(psim.M/psim.N)) * ...
            psim.cov_c_prime_diag + ...
            psim.var_c_prime_diag * eye(psim.M/psim.N);
        for j=1:psim.N
%             C_block = diag( wgn(psim.M/psim.N, 1, ...
%                 psim.var_c_prime_diag_db, 'complex') );
            C_block = diag(mvnrnd(zeros(1, psim.M/psim.N), ...
                cov_C_prime_diag) * (1/sqrt(2) + 1i /sqrt(2)));
            
            % Uncomment to check covariance effects between off-diagonal 
            % elements
%             C_block = C_block + ...
%                 reshape(mvnrnd(zeros(1, (psim.M/psim.N)^2), ...
%                 ones(psim.M^2/psim.N^2) * psim.var_c_prime_off) * ...
%                 (1/sqrt(2) + 1i /sqrt(2)), ...
%                 [psim.M/psim.N psim.M/psim.N]);
            
            C_block = C_block + ...
                wgn( psim.M/psim.N, psim.M/psim.N, ...
                psim.var_c_prime_off_db, 'complex' ) .* ...
                ( ones(psim.M/psim.N) - eye(psim.M/psim.N) );
            
            C_prime = blkdiag(C_prime, C_block);
        end
        C = eye(psim.M) + C_prime;
        
        psim.G_dl(:, :, i) = (C * psim.G_ul(:, :, i) * A).';
    end
end

