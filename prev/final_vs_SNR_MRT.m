clear all


%%%%%%%%%%%%%%%%%%%
T = 196;

num_iterations = 2500;

time_samples = 300;

B = 1; % Only supports one BS at this time
K_vector = B*5;

Nux = 4;
Nbx = 100;

SNR_dB = -20:5:30;

p_u_dB = 0;
t_u = K_vector*Nux;


UE_NRC_var_diag = -20;
UE_NRC_var_offdiag = -30;
UE_NRC_cov_diag = -inf;
UE_NRC_cov_offdiag = -inf;
UE_NRC_cov_diag_offdiag = -inf;

BS_NRC_var_diag = UE_NRC_var_diag;
BS_NRC_var_offdiag = UE_NRC_var_offdiag;
BS_NRC_cov_diag = UE_NRC_cov_diag;
BS_NRC_cov_offdiag = UE_NRC_cov_offdiag;
BS_NRC_cov_diag_offdiag = UE_NRC_cov_diag_offdiag;
%%%%%%%%%%%%%


if ~isscalar(K_vector) && ~isscalar(SNR_dB)
    error('Either number of users (K) or SNR (SNR_dB) should be a scalar.');
end

if B > 1
    error('Currently, only single-cell scenario is supported!');
end


p_u = 10^(p_u_dB/10);

%%% Simulate for each SNR point
LENGTH = length(SNR_dB);

SINR_sim_all = zeros(LENGTH,1);
SINR_n_sim_all = zeros(LENGTH,1);
num_sim_all = zeros(LENGTH,1);
num_n_sim_all = zeros(LENGTH,1);
denom_sim_all = zeros(LENGTH,1);
denom_n_sim_all = zeros(LENGTH,1);
SINR_math_all = zeros(1,LENGTH);
SINR_n_math_all = zeros(1,LENGTH);


% A_struct = load('correlated_C_prime_N_4_realiz_50000_noCov.mat');
% C_struct = load('correlated_C_prime_N_100_realiz_10000_noCov.mat');
% UE_NRC_matrices = A_struct.C_prime_tot;
% BS_NRC_matrices = C_struct.C_prime_tot;

% MU_BS = zeros(1,Nbx*Nbx);
% SIGMA_BS = BS_NRC_cov_matrix(Nbx, 10^(BS_NRC_var_diag/10), 10^(BS_NRC_var_offdiag/10), 10^(BS_NRC_cov_diag/10), 10^(BS_NRC_cov_offdiag/10), 10^(BS_NRC_cov_diag_offdiag/10));
% MU_UE = zeros(1,Nux*Nux);
% SIGMA_UE = BS_NRC_cov_matrix(Nux, 10^(UE_NRC_var_diag/10), 10^(UE_NRC_var_offdiag/10), 10^(UE_NRC_cov_diag/10), 10^(UE_NRC_cov_offdiag/10), 10^(UE_NRC_cov_diag_offdiag/10));


for ind = 1:LENGTH
    
    N0_dB = 0;
    p_d_db = SNR_dB(ind);
    p_d = 10^(p_d_db/10);
    
    K = K_vector;
    
    % Iterate
    for realiz = 1:num_iterations
        % Status display
        disp(['Iteration: ', num2str(realiz), '/', num2str(num_iterations), ', Point: ', num2str(ind), '/', num2str(LENGTH)]);
        
        
        M = wgn(Nux*K,Nbx,0,'complex');
        chan_total = wgn(Nux*K,Nbx,0,'complex');
        chan_total_err = (t_u*p_u/(1+t_u*p_u))*chan_total + (sqrt(t_u*p_u)/(1+t_u*p_u))*M;
        
        if Nux == 1
            A = eye(K*Nux) + diag((sqrt(10^(UE_NRC_var_diag/10)*6)) * (rand(1,K*Nux)-0.5 + 1i*(rand(1,K*Nux)-0.5))); % uniform distribution without cross correlation
%             A = diag(ones(1,K*Nux)+wgn(1,K*Nux,UE_NRC_var_diag,'complex'));
        else
            A_prime = [];
            for i = 1:K
                A_prime = blkdiag(A_prime, diag((sqrt(10^(UE_NRC_var_diag/10)*6)) * (rand(1,Nux)-0.5 + 1i*(rand(1,Nux)-0.5))) + (ones(Nux)-eye(Nux)).*((sqrt(10^(UE_NRC_var_offdiag/10)*6)) * (rand(Nux,Nux)-0.5 + 1i*(rand(Nux,Nux)-0.5)))); % uniform distribution without cross correlation
%                 A_prime = blkdiag(A_prime,(UE_NRC_matrices(:,:,(realiz-1)*2*K + i*2-1) + 1i*UE_NRC_matrices(:,:,(realiz-1)*2*K + i*2))/sqrt(2));
            end
            A = eye(K*Nux) + A_prime;
        end
        C = eye(Nbx) + diag((sqrt(10^(BS_NRC_var_diag/10)*6)) * (rand(1,Nbx)-0.5 + 1i*(rand(1,Nbx)-0.5))) + (ones(Nbx)-eye(Nbx)).*((sqrt(10^(BS_NRC_var_offdiag/10)*6)) * (rand(Nbx,Nbx)-0.5 + 1i*(rand(Nbx,Nbx)-0.5))); % uniform distribution without cross correlation
%         C = eye(Nbx) + (1i*BS_NRC_matrices(:,:,realiz*2-1) + 1i*BS_NRC_matrices(:,:,realiz*2))/sqrt(2);
%         A_prime = [];
%         for i = 1:K
%             A_prime_temp = mvnrnd(MU_UE,SIGMA_UE);
%             A_prime = blkdiag(A_prime,reshape(A_prime_temp,Nux,Nux).');
%         end
%         A = eye(K*Nux) + A_prime;
%         C_prime = mvnrnd(MU_BS,SIGMA_BS);
%         C = eye(Nbx) + reshape(C_prime,Nbx,Nbx).';
        
        chan_total_n = A*chan_total*C;
        
        X = wgn(K*Nux,time_samples,0,'complex');
        
        
        % Generate precoders and receive beamformers
        W = chan_total';
        beta = sqrt(((1+t_u*p_u)/(t_u*p_u))*(1/(Nbx*K*Nux)));
        
        W_err = chan_total_err';
        
        noise = wgn(K*Nux,time_samples,N0_dB,'complex');
        
        R = beta*sqrt(p_d)*chan_total*W_err*X + noise;
        R_n = beta*sqrt(p_d)*chan_total_n*W_err*X + noise;
        
        useful = sqrt(t_u*p_u*Nbx/(K*Nux*(1+t_u*p_u)))*sqrt(p_d)*X;
        useless = R - useful;
        useful_n = useful;
        useless_n = R_n - useful_n;

        
        num_sim_all(ind,realiz) = mean(var(useful.'));
        num_n_sim_all(ind,realiz) = mean(var(useful_n.'));
        
        denom_sim_all(ind,realiz) = mean(var(useless.'));
        denom_n_sim_all(ind,realiz) = mean(var(useless_n.'));
        
    end
    
    SINR_math = (Nbx/(K*Nux))*(p_d*t_u*p_u/((p_d+1)*(t_u*p_u+1)));
    SINR_math_all(ind) = SINR_math;
    
    
    beta_2 = ((1+t_u*p_u)/(t_u*p_u))*(1/(Nbx*K*Nux));
    G = t_u*p_u/(1+t_u*p_u);
    var_A_kk = 10^(UE_NRC_var_diag/10);
    trace_R_ak = 10^(UE_NRC_var_diag/10) + (Nux-1)*10^(UE_NRC_var_offdiag/10);
    trace_R_cd = Nbx * 10^(BS_NRC_var_diag/10);
    trace_R_cod = Nbx*(Nbx-1) * 10^(BS_NRC_var_offdiag/10);
    sum_R_cd = trace_R_cd + Nbx*(Nbx-1) * 10^(BS_NRC_cov_diag/10);
    
    var_I_SI = p_d*beta_2*G^2*Nbx*(1 + Nbx*var_A_kk + trace_R_ak) ...
        + p_d*beta_2*G^2*((1+var_A_kk)*sum_R_cd + (1+trace_R_ak)*(trace_R_cd + trace_R_cod)) ...
        + p_d*beta_2*(G/(1+t_u*p_u))*(1+trace_R_ak)*(Nbx + trace_R_cd + trace_R_cod);
    var_I_ISI = p_d*beta_2*G^2*( (1 + var_A_kk + (K*Nux-2)*(1+trace_R_ak))*(Nbx + trace_R_cd + trace_R_cod) + (trace_R_ak - var_A_kk)*(Nbx^2+sum_R_cd) ) ...
        + p_d*beta_2*(G/(1+t_u*p_u))*(K*Nux-1)*(1+trace_R_ak)*(Nbx + trace_R_cd + trace_R_cod);
    
    SINR_n_math = (p_d*(Nbx/(K*Nux))*G)/(var_I_SI + var_I_ISI + 1);
    SINR_n_math_all(ind) = SINR_n_math;
end


figure
plot(SNR_dB,(1-t_u/T)*(K*Nux)*log2(1+((Nbx/(K*Nux))*10.^(SNR_dB/10)./(1 + 10.^(SNR_dB/10)))),'b-+')
hold on
grid on
plot(SNR_dB,(1-t_u/T)*(K*Nux)*log2(1+SINR_math_all),'k-<')
plot(SNR_dB,(1-t_u/T)*(K*Nux)*log2(1+(mean(num_sim_all,2)./mean(denom_sim_all,2))),'k-o')
plot(SNR_dB,(1-t_u/T)*(K*Nux)*log2(1+SINR_n_math_all),'r-.*')
plot(SNR_dB,(1-t_u/T)*(K*Nux)*log2(1+(mean(num_n_sim_all,2)./mean(denom_n_sim_all,2))),'r-.o')
xlabel('SNR (dB)')
ylabel('Sum-rate')
legend('Ideal','Estimation error: Analytical','Estimation error: Simulated','Estimation error + NRC: Analytical','Estimation error + NRC: Simulated','Location','Best')
name = [num2str(B), '-', num2str(Nbx),'x BSs, ', num2str(K), '-', num2str(Nux),'x UEs', ' - sumRate - MRT.fig'];
saveas(gcf, name);
