clear all
clc

% Parameters for the simulation
psim = struct;

% Simulation parameters
psim.realizations = 3000;
psim.frequency = 1900; % [MHz]
psim.bandwidth = 20; % [MHz]
psim.coherence_interval = 500; % [Samples]
psim.nf_db = 9; % Noise figure

% MIMO parameters
psim.K = 20; % Number of UEs
psim.M = 100; % Number of total antennas in APs
psim.N = 100; % Number of APs

psim.power_control = 'per-ap';

% Uplink & Downlink parameters
psim.tu = psim.K; % Pilot time [Samples]
% Normalized transmit SNRs. Total power transmited
psim = set_rhos_tx(psim, psim.M * 100e-3, 100e-3);

% Channel parameters
psim.channel_model = 'hata-cost231';
psim.max_distance = 1000; % [m]

% Hata-COST231 parameters
psim.d1 = 50; % [m]
psim.d0 = 10; % [m]
psim.h_ap = 15; % [m]
psim.h_ue = 1.65; % [m]
psim.sigma_shadow_db = 8;
psim.sigma_shadow = 10^(psim.sigma_shadow_db/20);
psim.d_decorr = 100; % [m]
psim.delta_shadow = 0.5;
psim.shadowing_model = 'uncorrelated';

% Simulation starts here
var_a_diag_db = [-Inf -25 -20 -15];
var_c_off_db = [-Inf -Inf -Inf -Inf];
cov_c_diag_db = [-Inf -Inf -Inf -Inf];

var_c_diag_db = linspace(-40, -10, 10);

outrage_95 = zeros(length(var_c_diag_db), length(var_a_diag_db));
outrage_5 = zeros(length(var_c_diag_db), length(var_a_diag_db));
average_rate = zeros(length(var_c_diag_db), length(var_a_diag_db));

psim = gen_distances(psim);
psim = gen_channel_parameters(psim);
psim = set_var_nrc(psim, -Inf, -Inf, -Inf, -Inf);
[~, r_d] = compute_analytic_uatf(psim);
[ecdf_r_d, rate] = ecdf(r_d);

outrage_95_rc = interp1(ecdf_r_d, rate, 0.95, 'spline');
outrage_5_rc = interp1(ecdf_r_d, rate, 0.05, 'spline');
average_rate_rc = mean(r_d);

for i=1:length(var_c_diag_db)
    % Generate random UEs and APs positions and distances
    psim = gen_distances(psim);
    psim = gen_channel_parameters(psim);
    
    for j=1:length(var_a_diag_db)
        % NRC parameters
        % var_a_diag_db, var_c_diag_db, var_c_off_db, cov_c_diag_db
        psim = set_var_nrc(psim, var_a_diag_db(j), var_c_diag_db(i), ...
            var_c_off_db(j), cov_c_diag_db(j));
        
        [~, r_d] = compute_analytic_uatf(psim);
        [ecdf_r_d, rate] = ecdf(r_d);

        outrage_95(i, j) = interp1(ecdf_r_d, rate, 0.95, 'spline');
        outrage_5(i, j) = interp1(ecdf_r_d, rate, 0.05, 'spline');
        average_rate(i, j) = mean(r_d);
    end
end

line_styles = {'k-', 'k--', 'k:', 'k-.', 'kx'};

legends = {'\sigma_{a}^2 = -\infty dB'};
for i=2:length(var_a_diag_db)
    legends{end + 1} = ['\sigma_{a}^2 = ' ...
        num2str(var_a_diag_db(i)) ' dB'];
end

figure
hold on
grid on
for i=1:length(var_a_diag_db)
    plot(var_c_diag_db, outrage_95(:, i), line_styles{i});
end
xlabel('\sigma_{c}^2 (dB)')
ylabel('Per-User Rate at 95th Percentile (bit/s/Hz)')
legend(legends, 'Location', 'southwest')
saveas(gcf, './figures/fully_distributed/degradation_vs_C/degradation_vs_C_95.eps', ...
    'epsc')

figure
hold on
grid on
for i=1:length(var_a_diag_db)
    plot(var_c_diag_db, outrage_5(:, i), line_styles{i});
end
xlabel('\sigma_{c}^2 (dB)')
ylabel('Per-User Rate at 5th Percentile (bit/s/Hz)')
legend(legends, 'Location', 'southwest')
saveas(gcf, './figures/fully_distributed/degradation_vs_C/degradation_vs_C_5.eps', ...
    'epsc')

figure
hold on
grid on
for i=1:length(var_a_diag_db)
    plot(var_c_diag_db, average_rate(:, i), line_styles{i});
end
xlabel('\sigma_{c}^2 (dB)')
ylabel('Average Per-User Rate (bit/s/Hz)')
legend(legends, 'Location', 'southwest')
saveas(gcf, './figures/fully_distributed/degradation_vs_C/degradation_vs_C_ave.eps', ...
    'epsc')

figure
hold on
grid on
for i=1:length(var_a_diag_db)
    plot(var_c_diag_db, 100 * (average_rate_rc - average_rate(:, i)) ./ ...
        average_rate_rc, line_styles{i});
end
ylim([0 25])
xlabel('\sigma_{c}^2 (dB)')
ylabel('Average Per-User Rate Degradation (%)')
legend(legends, 'Location', 'northwest')
saveas(gcf, './figures/fully_distributed/degradation_vs_C/degradation_vs_C_ave_per.eps', ...
    'epsc')

figure
hold on
grid on
for i=1:length(var_a_diag_db)
    plot(var_c_diag_db, 100 * (outrage_95_rc - outrage_95(:, i)) ./ ...
        outrage_95_rc, line_styles{i});
end
ylim([0 25])
xlabel('\sigma_{c}^2 (dB)')
ylabel('Per-User Rate Degradation at 95th Percentile (%)')
legend(legends, 'Location', 'northwest')
saveas(gcf, './figures/fully_distributed/degradation_vs_C/degradation_vs_C_95_per.eps', ...
    'epsc')

figure
hold on
grid on
for i=1:length(var_a_diag_db)
    plot(var_c_diag_db, 100 * (outrage_5_rc - outrage_5(:, i)) ./ ...
        outrage_5_rc, line_styles{i});
end
ylim([0 25])
xlabel('\sigma_{c}^2 (dB)')
ylabel('Per-User Rate Degradation at 5th Percentile (%)')
legend(legends, 'Location', 'northwest')
saveas(gcf, './figures/fully_distributed/degradation_vs_C/degradation_vs_C_5_per.eps', ...
    'epsc')
