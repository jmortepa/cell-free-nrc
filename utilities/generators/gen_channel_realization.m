function [psim] = gen_channel_realization(psim, realization)
%GEN_CHANNEL_REALIZATION Summary of this function goes here
%   Detailed explanation goes here

    coeff_error = psim.betas(:, :, realization) - ...
        psim.gammas(:, :, realization);
    coeff_error = sqrt(coeff_error.' / 2);
    
    coeff_est = sqrt(psim.gammas(:, :, realization).' / 2);
    
    psim.G_ul_error = coeff_error .* ...
        (randn(psim.M, psim.K) + 1i * randn(psim.M, psim.K));
    
    psim.G_ul_est = coeff_est .* ...
        (randn(psim.M, psim.K) + 1i * randn(psim.M, psim.K));
    
    psim.G_ul = psim.G_ul_est + psim.G_ul_error;
    
    psim.G_dl_est = psim.G_ul_est.';
    
    A_prime = diag(wgn(psim.K, 1, psim.var_a_prime_db, 'complex'));
    A = eye(psim.K) + A_prime;
        
    if psim.N == psim.M
        C_prime = diag(wgn(psim.M, 1, psim.var_c_prime_diag_db, 'complex'));
        C = eye(psim.M) + C_prime;
    else
        C_prime = [];
        cov_C_prime_diag = (ones(psim.M / psim.N) - eye(psim.M / psim.N)) * ...
            psim.cov_c_prime_diag + psim.var_c_prime_diag * eye(psim.M/psim.N);
        for j=1:psim.N
            C_block = diag(mvnrnd(zeros(1, psim.M / psim.N), ...
                cov_C_prime_diag) * (1 / sqrt(2) + 1i / sqrt(2)));

            C_block = C_block + ...
                wgn( psim.M / psim.N, psim.M / psim.N, ...
                psim.var_c_prime_off_db, 'complex' ) .* ...
                ( ones(psim.M / psim.N) - eye(psim.M / psim.N) );

            C_prime = blkdiag(C_prime, C_block);
        end
        C = eye(psim.M) + C_prime;
    end
    
    psim.G_dl = (C * psim.G_ul * A).';
end

