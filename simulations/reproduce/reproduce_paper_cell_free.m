clear all
clc

% Parameters for the simulation
psim = struct;

% Simulation parameters
psim.realizations = 200;
psim.channel_realizations = 500;
psim.time_samples = 300;
psim.frequency = 1900; % [MHz]
psim.bandwidth = 20; % [MHz]
psim.coherence_interval = 200; % [Samples]
psim.nf_db = 9; % Noise figure

% MIMO parameters
psim.K = 20; % Number of UEs
psim.M = 100; % Number of total antennas in APs
psim.N = 100; % Number of APs

% Uplink & Downlink parameters
psim.tu = psim.K; % Pilot time [Samples]
% Normalized transmit SNRs. Total power transmited
psim = set_rhos_tx(psim, psim.M * 200e-3, 100e-3);

% NRC parameters
% var_a_diag_db, var_c_diag_db, var_c_off_db, cov_c_diag_db
psim = set_var_nrc(psim, -Inf, -Inf, -Inf, -Inf);

% Channel parameters
psim.channel_model = 'hata-cost231';
psim.max_distance = 1000; % [m]

% Hata-COST231 parameters
psim.d1 = 50; % [m]
psim.d0 = 10; % [m]
psim.h_ap = 15; % [m]
psim.h_ue = 1.65; % [m]
psim.sigma_shadow_db = 8;
psim.sigma_shadow = 10^(psim.sigma_shadow_db/10);
psim.d_decorr = 100; % [m]
psim.delta_shadow = 0.5;
psim.shadowing_model = 'correlated';

% Generate random UEs and APs positions and distances
psim = gen_distances(psim);

% Simulation starts here

% Simulation with correlated channel model
psim = gen_channel_parameters(psim);
[~, r_d] = compute_analytic_downlink(psim);

s_d = psim.bandwidth * ...
    (1 - (psim.tu/psim.coherence_interval) ) * r_d / 2;
[ecdf_s_d, throughput] = ecdf(s_d);

% Simulation with uncorrelated channel model
psim.shadowing_model = 'uncorrelated';
psim = gen_channel_parameters(psim);
[~, r_d_un] = compute_analytic_downlink(psim);
r_d_sim = compute_new_simulated(psim);

s_d_un = psim.bandwidth *...
    (1 - (psim.tu/psim.coherence_interval) ) * r_d_un / 2;
s_d_sim = psim.bandwidth *...
    (1 - (psim.tu/psim.coherence_interval) ) * r_d_sim / 2;

[ecdf_s_d_un, throughput_un] = ecdf(s_d_un);
[ecdf_s_d_sim, throughput_sim] = ecdf(s_d_sim);

figure
hold on
grid on
plot(throughput, ecdf_s_d, '--')
plot(throughput_un, ecdf_s_d_un)
legend('Correlated shadowing', 'Uncorrelated shadowing')
axis([0 20 0 1])
xlabel('Per-User Downlink Rate (Mbits/s)')
ylabel('Cumulative Distribution')

saveas(gcf, 'CDF_reproduce_paper_downlink', 'epsc')

figure
hold on
grid on
plot(throughput_un, ecdf_s_d_un)
plot(throughput_sim, ecdf_s_d_sim)
legend('Analytical', 'Simulated')
xlabel('Per-User Downlink Rate (Mbits/s)')
ylabel('Cumulative Distribution')