clear all
clc

% Parameters for the simulation
psim = struct;

% Simulation parameters
psim.realizations = 1;
psim.channel_realizations = 1000;
psim.time_samples = 300;
psim.coherence_interval = 196; % [Samples]
psim.progress_bar = 0;

% MIMO parameters
psim.K = 20; % Number of UEs
psim.M = 100; % Number of total antennas in APs
psim.N = 1; % Number of APs

% Uplink & Downlink parameters
psim.tu = psim.K; % Pilot time [Samples]

% Channel parameters
psim.channel_model = 'simple';
psim.max_distance = 1;

% NRC parameters sweep
var_a_diag_db = [-Inf, -20 -30];
var_c_diag_db = [-Inf, -20 -30];
var_c_off_db =  [-Inf, -30 -40];
cov_c_diag_db = [-Inf, -30 -40];

% SNR sweep
% rho_d_db = linspace(-20, 30, 10);

rho_d_db = linspace(10, 30, 3);

% Normalized transmit SNRs
psim = set_rhos_db(psim, 0, 0);

% Initialization
sinr_sim_vec = zeros(length(rho_d_db), length(var_a_diag_db));
sinr_analytic_vec = zeros(length(rho_d_db), length(var_a_diag_db));

% Generate Betas and Gammas
psim = gen_channel_parameters(psim);

for i=1:length(rho_d_db)
    for j=1:length(var_a_diag_db)
        display(['Iteration ' num2str((i-1)*length(var_a_diag_db) + j) ...
            ' of ' num2str(length(rho_d_db)*length(var_a_diag_db))])
        tic
        % Normalized transmit SNRs
        psim = set_rhos_db(psim, rho_d_db(i), 0);
        
        % NRC parameters
        % var_a_diag_db, var_c_diag_db, var_c_off_db, cov_c_diag_db
        psim = set_var_nrc(psim, var_a_diag_db(j), var_c_diag_db(j),...
            var_c_off_db(j), cov_c_diag_db(j));

        % Generate realizations of UL&DL channels
        psim = gen_channel_realizations(psim);

        % Simulate system
        r_d_sim = compute_simulated_uatf(psim);
        sinr_sim = 2 .^ (r_d_sim) - 1;
        sinr_sim = mean(sinr_sim);
%         sinr_sim = compute_simulated_downlink(psim);
        
        % Compute analytic SINR
        [sinr_analytic, ~] = compute_analytic_uatf(psim);

        sinr_sim_vec(i, j) = (sinr_sim);
        sinr_analytic_vec(i, j) = (sinr_analytic);
        toc
   end
end

str_var = '[\sigma_{a_{d}}^{2}, \sigma_{c_{d}}^{2}, \sigma_{c_{od}}^{2}, \delta_{c_{d}}^{2}] = ';
figure
hold on
grid on
plot(rho_d_db, log2( 1 + sinr_analytic_vec(:, 1)), '--')
plot(rho_d_db, log2( 1 + sinr_analytic_vec(:, 2)))
plot(rho_d_db, log2( 1 + sinr_analytic_vec(:, 3)), '-.')
plot(rho_d_db, log2( 1 + sinr_sim_vec), '*k')
xlabel('SNR (dB)')
ylabel('Spectral Efficiency (bits/s/Hz)')
legend('Reciprocal', [str_var '-[20, 20, 30, 30] dB'], [str_var '-[30, 30, 40, 40] dB'], 'Simulated')

% 
% 
% figure
% hold on
% grid on
% plot(rho_d_db, 10 * log10(sinr_sim_vec(:, 1)), '-')
% plot(rho_d_db, 10 * log10(sinr_analytic_vec(:, 1)), '*--')
% legend('Simulated', 'Analytical')
% xlabel('SNR (dB)')
% ylabel('SINR (dB)')
% 
% str_var = '[\sigma_{a_{d}}^{2}, \sigma_{c_{d}}^{2}, \sigma_{c_{od}}^{2}, \delta_{c_{d}}^{2}] = ';
% figure
% hold on
% grid on
% plot(rho_d_db, (1 - psim.tu / psim.coherence_interval) * psim.K * ...
%     log2( 1 + sinr_analytic_vec(:, 1)), '--')
% plot(rho_d_db, (1 - psim.tu / psim.coherence_interval) * psim.K * ...
%     log2( 1 + sinr_analytic_vec(:, 2)))
% plot(rho_d_db, (1 - psim.tu / psim.coherence_interval) * psim.K * ...
%     log2( 1 + sinr_analytic_vec(:, 3)), '-.')
% plot(rho_d_db, (1 - psim.tu / psim.coherence_interval) * psim.K * ...
%     log2( 1 + sinr_sim_vec), '*k')
% xlabel('SNR (dB)')
% ylabel('Spectral Efficiency (bits/s/Hz)')
% legend('Reciprocal', [str_var '-[20, 20, 30, 30] dB'], [str_var '-[30, 30, 40, 40] dB'], 'Simulated')
% 
% figure
% hold on
% grid on
% plot(rho_d_db, 100 * (sinr_analytic_vec(:, 1) - sinr_analytic_vec(:, 2)) ./ ...
%     sinr_analytic_vec(:, 1), 'o-')
% plot(rho_d_db, 100 * (sinr_analytic_vec(:, 1) - sinr_analytic_vec(:, 3)) ./ ...
%     sinr_analytic_vec(:, 1), 'x-')
% xlabel('SNR (dB)')
% ylabel('Relative SINR Degradation (%)')
% legend([str_var '-[20, 20, 30, 30] dB'], ...
%     [str_var '-[30, 30, 40, 40] dB'])
