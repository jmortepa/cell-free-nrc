clear all
clc

% Parameters for the simulation
psim = struct;

% Simulation parameters
psim.realizations = 100;
psim.channel_realizations = 500;
psim.frequency = 1900; % [MHz]
psim.bandwidth = 20; % [MHz]
psim.coherence_interval = 500; % [Samples]
psim.nf_db = 9; % Noise figure

% MIMO parameters
psim.K = 20; % Number of UEs
psim.M = 100; % Number of total antennas in APs
psim.N = 100; % Number of APs

psim.power_control = 'per-ap';

% Uplink & Downlink parameters
psim.tu = psim.K; % Pilot time [Samples]
% Normalized transmit SNRs. Total power transmited
psim = set_rhos_tx(psim, psim.M * 100e-3, 100e-3);

% Channel parameters
psim.channel_model = 'hata-cost231';
psim.max_distance = 1000; % [m]

% Hata-COST231 parameters
psim.d1 = 50; % [m]
psim.d0 = 10; % [m]
psim.h_ap = 15; % [m]
psim.h_ue = 1.65; % [m]
psim.sigma_shadow_db = 8;
psim.sigma_shadow = 10^(psim.sigma_shadow_db/20);
psim.d_decorr = 100; % [m]
psim.delta_shadow = 0.5;
psim.shadowing_model = 'uncorrelated';

% Generate random UEs and APs positions and distances
psim = gen_distances(psim);

% Simulation starts here
var_a_diag_db = -10;
var_c_diag_db = -10;
var_c_off_db = -Inf;
cov_c_diag_db = -Inf;

% NRC parameters
% var_a_diag_db, var_c_diag_db, var_c_off_db, cov_c_diag_db
psim = set_var_nrc(psim, var_a_diag_db, var_c_diag_db, ...
    var_c_off_db, cov_c_diag_db);

psim = gen_channel_parameters(psim);

r_d = compute_general_bound(psim);

[~, r_d_un] = compute_analytic_uatf(psim);

[ecdf_r_d, rate] = ecdf(r_d);
[ecdf_r_d_un, rate_un] = ecdf(r_d_un);

psim = set_var_nrc(psim, -Inf, -Inf, -Inf, -Inf);

r_d_rc = compute_general_bound(psim);

[~, r_d_un_rc] = compute_analytic_uatf(psim);

[ecdf_r_d_rc, rate_rc] = ecdf(r_d_rc);
[ecdf_r_d_un_rc, rate_un_rc] = ecdf(r_d_un_rc);

figure
hold on
grid on
plot(rate, ecdf_r_d, 'k-.')
plot(rate_un, ecdf_r_d_un, 'k--')
plot(rate_rc, ecdf_r_d_rc, 'k:')
plot(rate_un_rc, ecdf_r_d_un_rc, 'k')
xlabel('Per-User Rate (bit/s/Hz)')
ylabel('Cumulative Distribution')
var_str = ['[\sigma_{a}^2, \sigma_{c}^2] = [ ' ...
        num2str(var_a_diag_db) ... 
        ', ' num2str(var_c_diag_db) ' ] dB' ];
    
legend(['General ' var_str], ['UatF ' var_str], ...
    'General Bound - Reciprocal', 'UatF Bound - Reciprocal', ...
    'Location', 'southeast')

saveas(gcf, './figures/fully_distributed/general_bound/comparing_cdf_both.eps', ...
    'epsc')