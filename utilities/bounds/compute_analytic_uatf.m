function [sinr, r_d] = compute_analytic_uatf(psim)
%COMPUTE_ANALYTIC_SINR Summary of this function goes here
%   Detailed explanation goes here
    numerator = squeeze(psim.rho_d * sum(sqrt(psim.etas) .* psim.gammas, 2) .^ 2);
    
    sum_k_prime = sum(psim.etas .* psim.gammas, 1);
    sum_k_prime = repmat(sum_k_prime, [psim.K 1 1]);
    sum_k_prime_m = squeeze(sum(psim.betas .* sum_k_prime, 2));
    sum_m = squeeze(sum(psim.etas .* (psim.gammas .^ 2), 2));
    
    sum_m_sq = squeeze(sum(sqrt(psim.etas) .* psim.gammas, 2).^2);
    
    denominator = psim.rho_d * ...
        ((1 + psim.var_a_prime) * (1 + psim.trace_r_cm) * sum_k_prime_m + ...
        ((1 + psim.var_a_prime) * psim.sum_r_cm * sum_m) + ...
        psim.var_a_prime * sum_m_sq) + 1;

    sinr_all_real = numerator ./ denominator;
    sinr_all = sinr_all_real(:);
    
    sinr = mean(sinr_all);
    
    r_d = log2(1 + sinr_all);
end

