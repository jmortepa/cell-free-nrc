function [psim] = set_rhos_db(psim, rho_d_db, rho_u_db)
%SET_RHOS_DB Summary of this function goes here
%   Detailed explanation goes here
    psim.rho_u = 10^(rho_u_db/10);
    psim.rho_d = 10^(rho_d_db/10) / psim.M;
end

