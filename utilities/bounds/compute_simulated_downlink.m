function [sinr] = compute_simulated_downlink(psim)
%COMPUTE_SIMULATED_DOWNLINK Summary of this function goes here
%   Detailed explanation goes here
    numerator = zeros(psim.realizations, 1);
    denominator = zeros(psim.realizations, 1);
    for r=1:psim.realizations
        G_dl = psim.G_dl(:, :, r);
        G_dl_est = psim.G_dl_est(:, :, r);
        % Same etas and gammas for all realizations
        etas = psim.etas(:, :, 1);
        gammas = psim.gammas(:, :, 1);
        
        % Precoder matrix
        U = (sqrt(etas) .* G_dl_est)';
        % Data symbols
        Q = wgn(psim.K, psim.time_samples, 0, 'complex');
        % Noise matrix
        W = wgn(psim.K, psim.time_samples, 0, 'complex');
        % Received matrix
        R = sqrt(psim.rho_d) * G_dl * U * Q + W;
        % Useful signal matrix
        S = sqrt(psim.rho_d) * sum(sqrt(etas) .* gammas, 2) .* Q;
        % Non-Useful signal matrix
        NU = R - S;
        
        numerator(r) = mean(var(S.'));
        denominator(r) = mean(var(NU.'));
    end
    sinr = mean(numerator) ./ mean(denominator);

end

