function [psim] = set_var_nrc(psim, var_a_prime_db, var_c_prime_diag_db,...
    var_c_prime_off_db, cov_c_prime_diag_db)
%CHANGE_VAR_NRC Summary of this function goes here
%   Detailed explanation goes here
    psim.var_a_prime_db = var_a_prime_db;
    psim.var_c_prime_diag_db = var_c_prime_diag_db;
    psim.var_c_prime_off_db = var_c_prime_off_db;
    psim.cov_c_prime_diag_db = cov_c_prime_diag_db;

    psim.var_a_prime = 10^(psim.var_a_prime_db/10);
    psim.var_c_prime_diag = 10^(psim.var_c_prime_diag_db/10);
    psim.var_c_prime_off = 10^(psim.var_c_prime_off_db/10);
    psim.cov_c_prime_diag = 10^(psim.cov_c_prime_diag_db/10);
    
    psim.trace_r_cm = ((psim.M / psim.N) - 1) * psim.var_c_prime_off +...
            psim.var_c_prime_diag;
        
    psim.sum_r_cm = psim.var_c_prime_diag + ...
        ((psim.M / psim.N) - 1) * psim.cov_c_prime_diag;
    
    psim.var_alpha_trace = psim.var_a_prime + psim.trace_r_cm +...
        psim.var_a_prime * psim.trace_r_cm;
    
    psim.var_alpha_prime = psim.var_a_prime + psim.var_c_prime_diag +...
        psim.var_a_prime*psim.var_c_prime_diag;
end