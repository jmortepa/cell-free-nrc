clear all
clc

% Parameters for the simulation
psim = struct;

% Simulation parameters
psim.realizations = 100;
psim.channel_realizations = 500;
psim.frequency = 1900; % [MHz]
psim.bandwidth = 20; % [MHz]
psim.coherence_interval = 500; % [Samples]
psim.nf_db = 9; % Noise figure

psim.time_samples = 300;
psim.progress_bar = 1;

% MIMO parameters
psim.K = 20; % Number of UEs
psim.M = 100; % Number of total antennas in APs
psim.N = 1; % Number of APs

psim.power_control = 'per-ap';

% Uplink & Downlink parameters
psim.tu = psim.K; % Pilot time [Samples]

% Channel parameters
psim.channel_model = 'hata-cost231';
psim.max_distance = 1000; % [m]

% Hata-COST231 parameters
psim.d1 = 50; % [m]
psim.d0 = 10; % [m]
psim.h_ap = 15; % [m]
psim.h_ue = 1.65; % [m]
psim.sigma_shadow_db = 8;
psim.sigma_shadow = 10^(psim.sigma_shadow_db/20);
psim.d_decorr = 100; % [m]
psim.delta_shadow = 0.5;
psim.shadowing_model = 'uncorrelated';

% Generate random UEs and APs positions and distances
psim = gen_distances(psim);

psim = set_rhos_tx(psim, psim.M * 100e-3, 100e-3);

psim = gen_channel_parameters(psim);

var_a_diag_db = [-10];
var_c_diag_db = [-10 -10 -15 -20];
var_c_off_db = [-Inf -Inf -Inf -Inf];
cov_c_diag_db = [-Inf -Inf -Inf -Inf];

rate_uatf_vec = [];
ecdf_uatf_r_d_vec = [];

rate_general_vec = [];
ecdf_general_r_d_vec = [];

rate_perfect_vec = [];
ecdf_perfect_r_d_vec = [];

for i=1:length(var_a_diag_db)
    psim = set_var_nrc(psim, var_a_diag_db(i), var_c_diag_db(i), ...
        var_c_off_db(i), cov_c_diag_db(i));

    [r_d, r_d_perfect] = compute_general_bound(psim);
    
    [~, r_d_uatf] = compute_analytic_uatf(psim);
    
    [ecdf_r_d, rate] = ecdf(r_d);
    [ecdf_r_d_uatf, rate_uatf] = ecdf(r_d_uatf);
    [ecdf_r_d_perfect, rate_perfect] = ecdf(r_d_perfect);
    
    rate_general_vec = [rate_general_vec rate];
    ecdf_general_r_d_vec = [ecdf_general_r_d_vec ecdf_r_d];
    
    rate_uatf_vec = [rate_uatf_vec rate_uatf];
    ecdf_uatf_r_d_vec = [ecdf_uatf_r_d_vec ecdf_r_d_uatf];
    
    rate_perfect_vec = [rate_perfect_vec rate_perfect];
    ecdf_perfect_r_d_vec = [ecdf_perfect_r_d_vec ecdf_r_d_perfect];
    
end

line_styles = {'k-', 'k--', 'k:', 'k-.'};
[~, len] = size(rate_general_vec);

figure
hold on
grid on

plot(rate_general_vec(:, 1), ecdf_general_r_d_vec(:, 1), line_styles{2});
plot(rate_uatf_vec(:, 1), ecdf_uatf_r_d_vec(:, 1), line_styles{3});
plot(rate_perfect_vec(:, 1), ecdf_perfect_r_d_vec(:, 1), line_styles{1});

xlabel('Per-User Rate (bit/s/Hz)')
ylabel('Cumulative Distribution')

xlim([0 10])

legends = {'General Bound', 'UatF Bound', 'Perfect CSI Bound'};
legend(legends, 'Location', 'southeast')
% 
% figure
% hold on
% grid on
% for i=1:len
%     plot(rate_general_vec(:, i), ecdf_general_r_d_vec(:, i), line_styles{i});
% end
% xlabel('Per-User Rate (bit/s/Hz)')
% ylabel('Cumulative Distribution')
% 
% legends = {'General - Reciprocal'};
% for i=2:length(var_a_diag_db)
%     legends{end + 1} = ['General - [\sigma_{a}^2, \sigma_{c}^2] = [ ' ...
%         num2str(var_a_diag_db(i))... 
%         ', ' num2str(var_c_diag_db(i)) ' ] dB' ];
% end
% 
% legend(legends, 'Location', 'southeast')
% 
% figure
% hold on
% grid on
% for i=1:len
%     plot(rate_uatf_vec(:, i), ecdf_uatf_r_d_vec(:, i), line_styles{i});
% end
% xlabel('Per-User Rate (bit/s/Hz)')
% ylabel('Cumulative Distribution')
% 
% legends = {'UatF - Reciprocal'};
% for i=2:length(var_a_diag_db)
%     legends{end + 1} = ['UatF - [\sigma_{a}^2, \sigma_{c}^2] = [ ' ...
%         num2str(var_a_diag_db(i))... 
%         ', ' num2str(var_c_diag_db(i)) ' ] dB' ];
% end
% 
% legend(legends, 'Location', 'southeast')
% 
% figure
% hold on
% grid on
% for i=1:len
%     plot(rate_perfect_vec(:, i), ecdf_perfect_r_d_vec(:, i), line_styles{i});
% end
% xlabel('Per-User Rate (bit/s/Hz)')
% ylabel('Cumulative Distribution')
% 
% legends = {'Perfect CSI - Reciprocal'};
% for i=2:length(var_a_diag_db)
%     legends{end + 1} = ['Perfect CSI - [\sigma_{a}^2, \sigma_{c}^2] = [ ' ...
%         num2str(var_a_diag_db(i))... 
%         ', ' num2str(var_c_diag_db(i)) ' ] dB' ];
% end
% 
% legend(legends, 'Location', 'southeast')