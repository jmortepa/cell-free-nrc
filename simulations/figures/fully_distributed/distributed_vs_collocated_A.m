clear all
clc

% Parameters for the simulation
psim = struct;

% Simulation parameters
psim.realizations = 3000;
psim.frequency = 1900; % [MHz]
psim.bandwidth = 20; % [MHz]
psim.coherence_interval = 500; % [Samples]
psim.nf_db = 9; % Noise figure

% MIMO parameters
psim.K = 20; % Number of UEs
psim.M = 100; % Number of total antennas in APs
psim.N = 100; % Number of APs

psim.power_control = 'per-ap';

% Uplink & Downlink parameters
psim.tu = psim.K; % Pilot time [Samples]
% Normalized transmit SNRs. Total power transmited
psim = set_rhos_tx(psim, psim.M * 100e-3, 100e-3);

% Channel parameters
psim.channel_model = 'hata-cost231';
psim.max_distance = 1000; % [m]

% Hata-COST231 parameters
psim.d1 = 50; % [m]
psim.d0 = 10; % [m]
psim.h_ap = 15; % [m]
psim.h_ue = 1.65; % [m]
psim.sigma_shadow_db = 8;
psim.sigma_shadow = 10^(psim.sigma_shadow_db/20);
psim.d_decorr = 100; % [m]
psim.delta_shadow = 0.5;
psim.shadowing_model = 'uncorrelated';

% Simulation starts here

% Simulation starts here
var_c_diag_db = -15;
var_c_off_db = -Inf;
cov_c_diag_db = -Inf;

var_a_diag_db = linspace(-40, -10, 10);
N = [100 1];

outrage_95_rc = zeros(length(N), 1);
outrage_5_rc = zeros(length(N), 1);
average_rate_rc = zeros(length(N), 1);

outrage_95 = zeros(length(var_c_diag_db), length(N));
outrage_5 = zeros(length(var_c_diag_db), length(N));
average_rate = zeros(length(var_c_diag_db), length(N));

 for n=1:length(N)
    psim.N = N(n);
    % Generate random UEs and APs positions and distances
    psim = gen_distances(psim);
    psim = gen_channel_parameters(psim);

    psim = set_var_nrc(psim, -Inf, -Inf, -Inf, -Inf);

    [~, r_d] = compute_analytic_uatf(psim);
    [ecdf_r_d, rate] = ecdf(r_d);

    outrage_95_rc(n) = interp1(ecdf_r_d, rate, 0.95, 'spline');
    outrage_5_rc(n) = interp1(ecdf_r_d, rate, 0.05, 'spline');
    average_rate_rc(n) = mean(r_d);
    
    for j=1:length(var_a_diag_db)
        % NRC parameters
        % var_a_diag_db, var_c_diag_db, var_c_off_db, cov_c_diag_db
        psim = set_var_nrc(psim, var_a_diag_db(j), var_c_diag_db, ...
            var_c_off_db, cov_c_diag_db);

        [~, r_d] = compute_analytic_uatf(psim);
        [ecdf_r_d, rate] = ecdf(r_d);

        outrage_95(j, n) = interp1(ecdf_r_d, rate, 0.95, 'spline');
        outrage_5(j, n) = interp1(ecdf_r_d, rate, 0.05, 'spline');
        average_rate(j, n) = mean(r_d);
    end
 end


line_styles = {'k-', 'k--', 'k:', 'k-.', 'kx'};

legends = {};
for i=1:length(N)
    legends{end + 1} = ['\sigma_{c}^2 = ' ...
        num2str(var_c_diag_db) ' dB, N = ' num2str(N(i)) ];
end

figure
hold on
grid on
for n=1:length(N)
    plot(var_a_diag_db, 100 * (average_rate_rc(n) - ...
        average_rate(:, n)) ./ average_rate_rc(n), line_styles{n});
end
ylim([0 40])
xlabel('\sigma_{a}^2 (dB)')
ylabel('Average Per-User Rate Degradation (%)')
legend(legends, 'Location', 'northwest')
saveas(gcf, './figures/fully_distributed/distributed_vs_collocated_A/distributed_vs_collocated_A_ave_per.eps', ...
    'epsc')

figure
hold on
grid on
for n=1:length(N)
    plot(var_a_diag_db, 100 * (outrage_95_rc(n) - ...
        outrage_95(:, n)) ./ outrage_95_rc(n), line_styles{n});
end
ylim([0 40])
xlabel('\sigma_{a}^2 (dB)')
ylabel('Per-User Rate Degradation at 95th Percentile (%)')
legend(legends, 'Location', 'northwest')
saveas(gcf, './figures/fully_distributed/distributed_vs_collocated_A/distributed_vs_collocated_A_95_per.eps', ...
    'epsc')

figure
hold on
grid on
for n=1:length(N)
    plot(var_a_diag_db, 100 * (outrage_5_rc(n) - ...
        outrage_5(:, n)) ./ outrage_5_rc(n), line_styles{n});
end
ylim([0 40])
xlabel('\sigma_{a}^2 (dB)')
ylabel('Per-User Rate Degradation at 5th Percentile (%)')
legend(legends, 'Location', 'northwest')
saveas(gcf, './figures/fully_distributed/distributed_vs_collocated_A/distributed_vs_collocated_A_5_per.eps', ...
    'epsc')