function [r_d] = compute_simulated_uatf(psim)
%COMPUTE_NEW_SIMULATED Summary of this function goes here
%   Detailed explanation goes here

r_d = zeros(psim.K, psim.realizations);

if psim.progress_bar == 1
    progress_bar = waitbar(0, 'Simulation in progress...');
end

for realization=1:psim.realizations
    denominator = zeros(psim.channel_realizations, psim.K);
    
    received = zeros(psim.channel_realizations, psim.K);
    
    channel_statistics = sqrt(psim.rho_d) * ...
        sum(sqrt(psim.etas(:, :, realization)) .* ...
        psim.gammas(:, :, realization), 2);
    
    for channel_realization=1:psim.channel_realizations
        psim = gen_channel_realization(psim, realization);
        
        % Precoder matrix
        U = (sqrt(psim.etas(:, :, realization)) .* psim.G_dl_est)';
        % Data symbols
        Q = wgn(psim.K, psim.time_samples, 0, 'complex');
        % Noise matrix
        W = wgn(psim.K, psim.time_samples, 0, 'complex');
        % Received matrix
        R = sqrt(psim.rho_d) * psim.G_dl * U * Q + W;
        % Useful signal matrix
        S = channel_statistics .* Q;
        % Non-Useful signal matrix
        NU = R - S;
        
        denominator(channel_realization, :) = var(NU.');
        received(channel_realization, :) = var(R.');
    end
    
    n_r_d = log2(1 + ((channel_statistics.') .^ 2) ./ ...
        (mean(received) - ((channel_statistics.') .^ 2)));
    
    r_d(:, realization) = log2(1 + ((channel_statistics.') .^ 2) ./ ...
        (mean(denominator)));
    
    if psim.progress_bar == 1
        if isempty(findobj(progress_bar))
            progress_bar = waitbar((realization - 1) / ...
                (psim.realizations), 'Simulation in progress...');
        else
            waitbar((realization - 1) / (psim.realizations), ...
                progress_bar, 'Simulation in progress...');
        end
    end
end

r_d = r_d(:);

if psim.progress_bar == 1
    close(progress_bar);
end
end

