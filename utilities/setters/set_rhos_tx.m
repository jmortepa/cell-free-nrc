function [psim] = set_rhos_tx(psim, linear_tx_d_power,...
    linear_tx_u_power)
%SET_RHOS_TX Summary of this function goes here
%   Detailed explanation goes here
    T0 = 290; % [K]
    k_B = 1.381e-23; % Boltzmann constant [J/K]
    
    noise_power = psim.bandwidth * 1e6 * k_B * T0 * 10^(psim.nf_db/10);
    
    psim.noise_power = noise_power;
    
%     psim.noise_power = 10 ^ (94/10);
    
    psim.rho_d = (linear_tx_d_power / noise_power) / psim.M;
    psim.rho_u = (linear_tx_u_power / noise_power);

end

