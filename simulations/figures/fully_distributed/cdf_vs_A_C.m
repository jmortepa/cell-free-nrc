clear all
clc

% Parameters for the simulation
psim = struct;

% Simulation parameters
psim.realizations = 100;
psim.frequency = 1900; % [MHz]
psim.bandwidth = 20; % [MHz]
psim.coherence_interval = 500; % [Samples]
psim.nf_db = 9; % Noise figure

% MIMO parameters
psim.K = 20; % Number of UEs
psim.M = 100; % Number of total antennas in APs
psim.N = 100; % Number of APs

psim.power_control = 'per-ap';

% Uplink & Downlink parameters
psim.tu = psim.K; % Pilot time [Samples]
% Normalized transmit SNRs. Total power transmited
psim = set_rhos_tx(psim, psim.M * 100e-3, 100e-3);

% Channel parameters
psim.channel_model = 'hata-cost231';
psim.max_distance = 1000; % [m]

% Hata-COST231 parameters
psim.d1 = 50; % [m]
psim.d0 = 10; % [m]
psim.h_ap = 15; % [m]
psim.h_ue = 1.65; % [m]
psim.sigma_shadow_db = 8;
psim.sigma_shadow = 10^(psim.sigma_shadow_db/20);
psim.d_decorr = 100; % [m]
psim.delta_shadow = 0.5;
psim.shadowing_model = 'uncorrelated';

% Generate random UEs and APs positions and distances
psim = gen_distances(psim);

% Simulation starts here
var_a_diag_db = [-Inf -10 -15 -20];
var_c_diag_db = [-Inf -10 -15 -20];
var_c_off_db = [-Inf -Inf -Inf -Inf];
cov_c_diag_db = [-Inf -Inf -Inf -Inf];

rate_vec = [];
ecdf_r_d_vec = [];
outrage = zeros(length(var_a_diag_db), 1);
for i=1:length(var_a_diag_db)
    % NRC parameters
    % var_a_diag_db, var_c_diag_db, var_c_off_db, cov_c_diag_db
    psim = set_var_nrc(psim, var_a_diag_db(i), var_c_diag_db(i), ...
        var_c_off_db(i), cov_c_diag_db(i));
    psim = gen_channel_parameters(psim);
    
    [~, r_d] = compute_analytic_uatf(psim);
    [ecdf_r_d, rate] = ecdf(r_d);
    
    outrage(i) = interp1(ecdf_r_d, rate, 0.95, 'spline');
    
    rate_vec = [rate_vec rate];
    ecdf_r_d_vec = [ecdf_r_d_vec ecdf_r_d];
end

line_styles = {'k-', 'k--', 'k:', 'k-.', 'kx'};

figure
hold on
grid on
[~, len] = size(rate_vec);
for i=1:len
    plot(rate_vec(:, i), ecdf_r_d_vec(:, i), line_styles{i});
end
xlabel('Per-User Rate (bit/s/Hz)')
ylabel('Cumulative Distribution')

legends = {'Reciprocal'};
for i=2:length(var_a_diag_db)
    legends{end + 1} = ['[\sigma_{a}^2, \sigma_{c}^2] = [ ' ...
        num2str(var_a_diag_db(i))... 
        ', ' num2str(var_c_diag_db(i)) ' ] dB' ];
end

legend(legends, 'Location', 'southeast')
saveas(gcf, './figures/fully_distributed/cdf_vs_A_C/cdf_vs_A_C.eps', ...
    'epsc')
